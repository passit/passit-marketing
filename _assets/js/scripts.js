document.addEventListener('DOMContentLoaded', function() {
	var thanks = document.querySelector('.mm-signed-up');
	if (thanks) {
		var checkGet = parseUrlGets('signed-up');
		if (checkGet) {
			thanks.classList.add('mm-signed-up--active');
		}
	}

	checkMainNavPosition();
});

document.addEventListener('scroll', function() {
	checkMainNavPosition();
});


// Related to loading values from a URL.
// http://stackoverflow.com/a/5448595/
var parseUrlGets = function(value) {
	var result = false;
	var tempArray = [];

	var gets = window.location.search.substr(1).split('&');
	gets.forEach(function (item) {
		tempArray = item.split('=');
		if (tempArray[0] === value) {
			result = decodeURIComponent(tempArray[1]);
		}
	});
	return result;
};

// Fix the navbar to the top once we've scrolled past the top of the page
var checkMainNavPosition = function() {
	var mainNav = document.querySelector('.main-nav');

	if (window.pageYOffset > mainNav.offsetHeight) {
		mainNav.classList.add('main-nav--fixed');
	} else {
		mainNav.classList.remove('main-nav--fixed');
	}
}
