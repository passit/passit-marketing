---
layout: post
title:  "Passit 1.10: Backup Codes for Account Recovery"
author: David Burke
date:   2018-12-26 00:00:00 -0400
categories: release
excerpt: >
    Passit 1.10 has been released. It features a backup code that allows you to recover your account if you forget your master password.
---

Passit 1.10 has been released. It features a backup code that allows you to recover your account if you forget your master password.

<div style="width: 600px; max-width: 100%; margin: 0 auto; border: 1px solid #D0E6E6;"><img alt="Sample backup code shown inside of PDF" title="Sample backup code shown inside of PDF" src="/images/blog/backup-code.png" /></div>

If you already have an account, there are two ways to get a backup code, both from your account page:

1. Select “Change Backup Code”. Once you verify your password, you will be able to download your backup code in a PDF. 
1. Select “Change Account Password & Backup Code”. You will always get a new backup code when you change your password.

Your backup code should be printed out or saved in a secure digital location.

If you forget your master password, you will see a link that says “recover your account” on the login page. You will be prompted to enter your email address and then sent an email with a link to enter your backup code.

Make sure to keep your code safe. If an attacker gets your backup code and gains access to your email account, they could log into your Passit account.

**Good places to store your backup code:**
- A safe
- External hard drive
- A folder that cannot be shared on your hard drive

**Bad places to store your backup code:**
- Anywhere in your email account
- A cloud drive that isn't encrypted
- Your downloads folder

We also added faster startup times on the web extension popup, which will make a big difference if you have a lot of passwords.

Our next release, 1.11, will focus on feature parity with our Android app.
