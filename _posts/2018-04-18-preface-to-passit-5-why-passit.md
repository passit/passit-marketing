---
layout: post
title: "A Preface to Passit, Part 5: Why pick Passit?"
author: Brendan Berkley
excerpt: >
    We've posted about why we like password managers, open source software,
    and cloud-based services. Conveniently, Passit is all of those things.
date: 2018-04-18 00:00:00 -0400
categories: ideas
---


*This is the fifth installment of a five-part series. Links to the other posts can be found at the end of this&nbsp;article.*

In this series, I've been building a case for Passit. I first noted that, in a world with very real and dangerous online threats, it is good to take steps to secure yourself that [strike a good balance between liberty and safety]({% post_url 2018-03-05-preface-to-passit-1-liberty-vs-safety %}). I then wrote about how [password managers provide that balance]({% post_url 2018-03-15-preface-to-passit-2-why-a-password-manager %}). Then, I wrote about the merits of [open source software]({% post_url 2018-03-28-preface-to-passit-3-why-open-source %}) and [cloud-based services]({% post_url 2018-04-05-preface-to-passit-4-why-cloud-based %}).

If I was convincing in those posts, then the only thing left is to convince you that Passit is the right open source, cloud-based password manager.

## Why Passit?

1. **The most popular solutions are not cloud-based *and* open source.** 1Password, LastPass, Dashlane, and RoboForms don't let you see their source. Some of the popular open source password managers&mdash;KeePass comes to mind&mdash;aren't cloud-based.
2. **We're built on the cutting edge of Web technology.** Some great advancements have been made in the last few years to make it easier to practice good security that works fast on the latest browsers.
3. **It's better than nothing.** This doesn't seem like a great slogan to hang your hat on, but we live in a world where over 90% of people don't use a password manager. In that world, what we're offering is almost certainly going to be a better solution than what most people are doing now.
4. **It's good for business.** If you're managing credentials on three different Google Sheets, or if your Project Managers all have their own systems, or if you're constantly e-mailing clients for credentials and getting them sent over email or Slack in plaintext, then Passit is going to be better for you.
5. **It's way better for your family.** Ever had your spouse or your child ask for a password? This makes it easier and more secure to share. Ever get that guilt trip about you dying suddenly and no one knowing your passwords? Just have a way for your family to know your master password, and then they have the keys to the kingdom.
6. **Data portability.** This goes beyond the benefits of a cloud-based solution where you can access your secrets anywhere and switch to new devices easily. We want to make sure that you're not being tied to any particular operating system or web browser either. While we do have some minimum browser requirements due to the tech we chose to build Passit, we're striving to make sure that you never feel like Passit is the driving force for your technology choices. We already have extensions for [Firefox]({{ site.firefox_extension_url }}) and [Chrome]({{ site.chrome_extension_url }}), and we're hoping to release an Android app soon.

## Try it out!

[Sign up]({{ site.web_app_url }}register) to get started. If you want to import data after you're registered, head over to the [import page]({{ site.web_app_url }}import).

You can also check out our extensions and app:

- [Passit for Firefox]({{ site.firefox_extension_url }}) &ndash; Direct link
- [Passit for Chrome]({{ site.chrome_extension_url }}) &ndash; Chrome Web Store
- [Passit for Android]({{ site.android_app_url }}) &ndash; Google Play

### A Preface to Passit:

- [Part 1: Liberty vs. safety in an imperfect world]({% post_url 2018-03-05-preface-to-passit-1-liberty-vs-safety %})
- [Part 2: Why use a password manager?]({% post_url 2018-03-15-preface-to-passit-2-why-a-password-manager %})
- [Part 3: Why use an open source app?]({% post_url 2018-03-28-preface-to-passit-3-why-open-source %})
- [Part 4: Why use a cloud-based password manager?]({% post_url 2018-04-05-preface-to-passit-4-why-cloud-based %})
- Part 5: Why pick Passit?
