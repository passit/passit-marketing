FROM ruby:2.4

RUN apt-get update && \
    apt-get install -y nodejs npm gcc git rsync make && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN gem install jekyll && \
	gem install bundler && \
    npm install gulp gulpfile-install -g

RUN         mkdir /code && mkdir /code/code
WORKDIR     /code/code
ADD     .   /code/code/

RUN bundle install
