---
layout: page
title: "Passit Private Beta"
---

The goal of the private beta is to see how it works with real users. We'd like to work out all the kinks before making a more public release.

While "private beta" is the colloquial term we're using, it's really more "unlisted" than "private". Feel free to share Passit with others however you'd like, but we'd prefer if you linked to [Passit's home page](https://passit.io) so others can try this out under the same assumptions that you are.

## Site information

Visit [{{ site.web_app_url }}register]({{ site.web_app_url }}register) to get started. If you want to import data once you're registered, head over to the [import page]({{ site.web_app_url }}import). 

## Install extensions

- [Passit for Firefox]({{ site.firefox_extension_url }}). This will link you directly to the extension for downloading.
- [Passit for Chrome]({{ site.chrome_extension_url }}). This is a link to the Chrome Web Store.

## Support/Issues

Email [info@passit.io](mailto:info@passit.io). If you'd rather report issues at our code repository, go to [Passit on Gitlab](https://gitlab.com/passit/passit/issues).

## A note on emails from us

By signing up for the private beta, you're consenting to receive emails from Passit. We need to make sure that we can contact you to let you know if there are any critical updates, and we need to be able to let you know when the private beta is ending. We might send some extra marketing emails out as well, but we're not currently planning to do a lot of that, particularly not during this phase of the process.
