---
layout: post
title:  "Passit 1.2.0 released"
date:   2017-12-09 17:39:00 -0500
categories: release
---

Passit 1.2.0 contains some infrastructure work to support server background tasks and the beginning of a invite to group system. Our integration tests now run multiple browsers that share secrets.

The frontend contains some minor bug fixes.

As always just pull down the latest docker image to upgrade. See [instructions](http://passit.io/install/).

## Roadmap

Passit 1.3 plans include allowing sharing on app.passit.io (Right now sharing is only enabled in private instances). See all 1.3 issues on our [gitlab](https://gitlab.com/groups/passit/-/issues?scope=all&utf8=%E2%9C%93&state=opened&milestone_title=v1.3).
