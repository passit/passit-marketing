---
layout: post
title: "A Preface to Passit, Part 1: Liberty vs. safety in an imperfect world"
author: Brendan Berkley
excerpt: >
    Why do password managers exist? This is a somewhat philosophical look at the
    world with Internet security in mind, and justification for a product that
    can help people feel safer online.
date: 2018-03-05 00:00:00 -0400
categories: ideas
---

*This is the first installment of a five-part series. Links to the other published posts can be found at the end of this&nbsp;article.*

In a perfect world, there would be no need for any kind of security on the Internet. You could simply go to your favorite site, tell it who you are, and instantly get access to your account.

However, this is not a perfect world. There are hackers who want to break into your accounts to steal your digital valuables, prankster friends who want to mess with you, family members or [nebby](http://pittsburghspeech.pitt.edu/PittsburghSpeech_PgheseOverview.html) neighbors who want to snoop around, kids who want to circumvent your rules, and those in law enforcement who are willing to achieve their goals at the expense of your personal rights.

In this imperfect world, most online services will use passwords to provide security. Passwords are digital locks for the digital doors that control access to your private spaces. And like a lock on your front door, they do a pretty good job at balancing priorities for you. Bad guys could still get into your house if they're really determined, but a good lock discourages the casual thief while keeping disruptions to your life at a minimum. Passwords safeguard your account in similar ways.

But have no doubt: these things are still disruptive. If you've ever lost or forgotten your keys, if you've ever traveled halfway to work before starting to stress about whether or not you locked the door, if you've ever had difficulties getting a friend inside your place when you weren't around to unlock the door, or if you've ever had a key stolen or a lock picked, then you've experienced some of the hassles. And if you've ever forgotten a password, had trouble picking a good password, had trouble navigating the login page, had difficulty getting a password you need from someone else, or had someone try to steal your password, then you've experienced troubles with passwords.

So, with respect to [misquotes of Ben Franklin](https://www.washingtonpost.com/news/volokh-conspiracy/wp/2014/11/11/liberty-safety-and-benjamin-franklin/), we do sacrifice liberty for safety. We recognize that when there are threats to freedom, we try to strike a balance between neutralizing the threat while maintaining as much of that freedom as we can.

With that in mind, I’d like to bring [Passit](https://passit.io) into the conversation.

Passit is an open source project that aims to build the most secure, trustworthy, and useful cloud-based password manager ever. That reads a bit like a stuffy mission statement, but there's nothing cliche about it. It defines our strengths and our ambitions. 

Over the next few posts in this series, I’ll answer some underlying questions: why use a password manager? Why care about open source? Why use cloud-based software? And, most importantly to us: why use Passit?

### A Preface to Passit:

- Part 1: Liberty vs. safety in an imperfect world
- [Part 2: Why use a password manager?]({% post_url 2018-03-15-preface-to-passit-2-why-a-password-manager %})
- [Part 3: Why use an open source app?]({% post_url 2018-03-28-preface-to-passit-3-why-open-source %})
- [Part 4: Why use a cloud-based password manager?]({% post_url 2018-04-05-preface-to-passit-4-why-cloud-based %})
- [Part 5: Why pick Passit?]({% post_url 2018-04-18-preface-to-passit-5-why-passit %})
