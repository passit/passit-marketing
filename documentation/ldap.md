---
layout: page
title: "LDAP Integration"
---

*These instructions primarily deal with self-hosted Passit. If you'd like to restrict access in your domain on our hosted app.passit.io service, please contact <a href="mailto:info@passit.io">info@passit.io</a> for managed services.*

LDAP integration enables the restriction of Passit usage to active LDAP users.

For example, a workplace IT group can deactivate all user privileges with one action. When the user attempts to log in to Passit, they are blocked after Passit checks LDAP to determine if the user is active.

This should work with any LDAP provider and has been tested with Foxpass and Jumpcloud. LDAP integration is currently a preview feature and requires access to Django Admin.

## Enable LDAP for specific domain

1. Enable Django Admin by setting the environment variable `ENABLE_DJANGO_ADMIN` to `True` (In the future, a Passit admin panel will remove the need for this).
2. Create a Django admin user by running `./manage.py createsuperuser` in your Passit docker container. For example, if using docker-compose this would be `docker-compose run --rm web ./manage.py createsuperuser`.
3. Go to `your-passit-domain/admin/` and log in with your new Django superuser account. Remember, normal Passit user accounts will not be able to log in here.
4. Click "Add Organization".
5. Give your organization a name and add a "LDAP Sync Setting". Fill out all required fields. Note that your LDAP Bind user password cannot be encrypted. Your Passit server must bind to your LDAP server to query for active users. This bind user account should have read only access to query a list of active users. 

<!-- [Could we get a tabbed widget here with specific instructions for Foxpass, Jumpcloud, others?] -->

Only users with the specified email domain will be affected by LDAP integration. When they log in, Passit will check to ensure the user exists in LDAP. External users (those with other domains) may still be able to log in. See the `PRIVATE_ORG_MODE_WHITELIST` setting if you'd like to restrict this further.