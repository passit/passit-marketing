'use strict';
 
var gulp = require('gulp');
var sass = require('gulp-sass');
var del = require('del');
var gulpCopy = require('gulp-copy');
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');
var sassLint = require('gulp-sass-lint');
// var svgSymbols = require('gulp-svg-symbols');


// If we're going to copy things from one place to another, we want to make sure
// things stay clean.
gulp.task('clean', function () {
	return del(['dist/images/**/*','dist/css/**/*']);
});

// The idea here is that the assets/ folder is never touched manually, and when
// I tweak the setup more it'll go away entirely. Committing duplicate images
// is bad in the short term, and committing compiled CSS is bad in the long term
// (merge conflict city). But done is better than perfect, so we'll do it for now.
gulp.task('copy', function (cb) {
	var directoriesToCopy = [
	    './_assets/icon/**/*',
	    './_assets/images/**/*',
	    './_assets/fonts/**/*',
	    './_assets/js/**/*'
	  ];
	return gulp.src(directoriesToCopy)
		.pipe(gulpCopy('./dist', { prefix:1 }));
});


gulp.task('sass-lint', function () {
	return gulp.src(['./_assets/sass/**/*.scss', '!./_assets/sass/vendor/**/*.scss'])
		.pipe(sassLint())
		.pipe(sassLint.format())
		.pipe(sassLint.failOnError())
});


gulp.task('sass', function () {
  return gulp.src('./_assets/sass/**/*.scss')
	.pipe(sourcemaps.init())
	.pipe(sass({
		// outputStyle: 'compressed'
	}).on('error', sass.logError))
	.pipe(autoprefixer({
		browsers: ['last 2 versions', 'IE 9'],
		cascade: false
	}))
	.pipe(sourcemaps.write('.'))
	.pipe(gulp.dest('./dist/css'));
});


// gulp.task('sass:watch', function () {
gulp.task('watch', function () {
	gulp.watch('./_assets/**/*', ['just-sass']);
	// gulp.watch('./_assets/**/*', ['default']);
});

// Inactive!
// gulp.task('sprites', function () {
//   return gulp.src('_assets/images/**/*.svg')
//     .pipe(svgSymbols({
//     	templates: ['./_layouts/svg-symbols.svg']
//     }))
//     .pipe(gulp.dest('_includes/'));
// });


gulp.task('default', ['clean', 'copy', 'sass-lint', 'sass']);

gulp.task('just-sass', ['sass-lint', 'sass']);