---
layout: page
title: "Contributing to Passit"
redirect_from:
  - /contributing/
---

This page is intended to give new contributors an overview of Passit. 
 
Passit is comprised of several Git repos, stored in our [GitLab group](https://gitlab.com/passit). You can find more specific and complete documentation in each project, but here is a summary of the key repositories that make Passit work:

- [passit-backend](https://gitlab.com/passit/passit-backend) - Python/Django - This provides an API and basic storage services for user data. The backend does no crypto, except in unit tests. It does provide basic group access control list features. Even if we didn't encrypt user data, the backend would provide a traditional server that ensures users are only allowed to create, edit, and view what they should have access to.
- [passit-frontend](https://gitlab.com/passit/passit-frontend) - TypeScript, Angular, NativeScript, and ngrx/store (Redux) - This project is our user interface. It powers our web app and browser extensions.
- We also have a [diagram of our basic security model](/security-model/).

## Reporting issues

If you have a bug or feature request to report - you may report it gitlab project where the issue is found. If the issue involves multiple parts or you don't know where it belongs - you may report it to the [Passit meta repo](https://gitlab.com/passit/passit).

### Making a good bug report

Include steps to reproduce the issue, what you expected to happen, and what happened instead.

### Making a good merge request

Passit has pretty good test coverage. When you make a change or fix a bug, you should add or modify a unit test to show how your change works. For example if you fixed a bug - the unit test should fail before your patch and pass after it.

## What if I'm not a programmer?

If you want to work on Passit but nothing above sounds interesting or accessible to you, that's okay! There are ways you could get involved:

### Security auditing

Auditing/penetration testing is really great for us; the more we get, the more secure and reputable we will be.

### Design

We'd be interested in help with regards to UX design in particular. If you have a good sense of what makes a web app work well and you're good at expressing those ideas through wireframing, we'd be interested in your help.

### Marketing

If you would like to be a public advocate for Passit, get in touch with us and we'll see what we can work out.

### QA

If you'd like to help us test future releases, all you'd need is the ability to access the Internet and a willingness to meticulously work through a spreadsheet.
