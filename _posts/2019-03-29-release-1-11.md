---
layout: post
title:  "Passit 1.11: LDAP initial support"
author: David Burke
date:   2019-03-29 00:00:00 -0400
categories: release
---

Passit 1.11 has been released. It features a preview of LDAP sync.

If you use Passit at work and want to restrict authentication to LDAP users, you can now do so. [Read more about it here](/documentation/ldap/).

This feature will eventually be coming to app.passit.io. If that is of interest to your company, please contact [info@passit.io](mailto:info@passit.io).

### Other notable changes

- The Android app now supports import and export.
- The Web extension autofill feature works with more sites (Now based on the [browserpass extension](https://github.com/browserpass/browserpass). Thanks, browerpass team!)
- 20% faster start up time thanks to Angular module lazy-loading. This is noticable in the web extension popup.
- Self hosted users can now enable the Django Admin interface via environment variable `ENABLE_DJANGO_ADMIN`.

### What's next

Our next release, 1.12, will focus on feature parity with our Android app and an improved interface for adding members to groups.

Don't forget to add a thumbs up to your most requested features in [GitLab](https://gitlab.com/groups/passit/-/issues)!
