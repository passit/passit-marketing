document.addEventListener('DOMContentLoaded', function() {
	var thanks = document.querySelector('.mm-signed-up');
	if (thanks) {
		var checkGet = parseUrlGets('signed-up');
		if (checkGet) {
			thanks.classList.add('mm-signed-up--active');
		}
	}

	checkMainNavPosition();
});

document.addEventListener('scroll', function() {
	checkMainNavPosition();
});


// Related to loading values from a URL.
// http://stackoverflow.com/a/5448595/
var parseUrlGets = function(value) {
	var result = false;
	var tempArray = [];

	var gets = window.location.search.substr(1).split('&');
	gets.forEach(function (item) {
		tempArray = item.split('=');
		if (tempArray[0] === value) {
			result = decodeURIComponent(tempArray[1]);
		}
	});
	return result;
};

// Fix the navbar to the top once we've scrolled past the top of the page
var checkMainNavPosition = function() {
	var mainNav = document.querySelector('.main-nav');

	if (window.pageYOffset > mainNav.offsetHeight) {
		mainNav.classList.add('main-nav--fixed');
	} else {
		mainNav.classList.remove('main-nav--fixed');
	}
}

// Email subscribe stuff.
var moonMailSubmit = function(event) {
	event.preventDefault();

	var URL = 'https://api.moonmail.io/lists/cj5cigh41000601pb7qr6a6s4/subscribe';
	var U = 'Z29vZ2xlLW9hdXRoMnwxMDc5MTk2MzIzODU5NTg2MTEzNDE';
	var email = document.getElementById('mm-email').value;
	var errorElement = document.getElementById("mm-message");

	if (email && email !== "" && email.match(/^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/)) {
		errorElement.textContent = "Submitting...";

		var request = new XMLHttpRequest();
		request.open('POST', URL, true);
		request.setRequestHeader('Content-Type', 'application/json');
		request.send(JSON.stringify({
			"u": U,
			"email": email,
		}));
		request.onreadystatechange = function() {
			if (request.readyState == 4) {
				if (request.status == 200) {
					errorElement.textContent = "Please check your email to confirm subscription.";
				} else if (request.status === 400) {
					try {
						var emailMessage = JSON.parse(request.responseText).message.email;
						if (emailMessage === "already exists") {
							errorElement.textContent = "This email address is on our list already. If you haven't confirmed it yet, check your email.";
						} else {
							errorElement.textContent = "Something went wrong. Please try again later.";
						}
					} catch(error) {
						// if for some reason response is not actually JSON
						errorElement.textContent = "Something went wrong. Please try again later.";
					}
				}
			}
		};
	} else {
		errorElement.textContent = "Please enter a valid email address.";
	}
}
