---
layout: post
title: "A Preface to Passit, Part 4: Why use a cloud-based password manager?"
author: Brendan Berkley
excerpt: >
    Cloud-based software travels with you, is available on any Internet-connected
    device, and has a lot of other perks. Some don't think it's ideal for security
    though, so we'll discuss why we think the pros outweigh the cons.
date: 2018-04-05 00:00:00 -0400
categories: ideas
---

*This is the fourth installment of a five-part series. Links to the other posts can be found at the end of this&nbsp;article.*

For the uninitiated, when we talk about running Passit and storing its data in the cloud, we mean that the app is operated on a computer other than your own, and you can use Passit anywhere you have an Internet connection and a proper Web browser.

Most reasons to go cloud-based instead of keeping everything on your computer center on convenience. First, you can get your passwords on different devices much more easily. If you end up at a new work computer or at the library or on a new phone, just log in and you have everything. Second, it opens the door for great features like sharing passwords in groups. While this can be done with locally-run systems, there's a lot less friction online. Third, you remove the risk of things like a dead drive in your computer, a theft, or a fire ruining your password list. While all of these things are possible in the cloud, we think that people who manage computers for a living and whose business is tied to how well they do it will do a better job than your average user.

However, if you have your own server and a great Internet connection, just run it on your own server and reap the first two benefits.

Going cloud-based introduces different security risks. We acknowledge them, concerns such as NSA warrants on hosted servers, or the possibility that we update the software without you being able to verify, or some concerns about transmitting data online, particularly on that coffee shop Wi-Fi that doesn't have a password. Some are also wary of vendor lock-in and holding data hostage.

While this post isn't intended to assuage all of those concerns in detail, we do believe that Passit's [security model](https://passit.io/security-model/) and [open source]({% post_url 2018-03-28-preface-to-passit-3-why-open-source %}) code reduces these risks enough that the benefits outweigh them.

Ultimately, the decision of a cloud-based system versus running on your local devices is best made by you and your specific needs, but for most people, it's hard to argue against the security, stability, and portability that comes from letting someone else's server do all the work.

### A Preface to Passit:

- [Part 1: Liberty vs. safety in an imperfect world]({% post_url 2018-03-05-preface-to-passit-1-liberty-vs-safety %})
- [Part 2: Why use a password manager?]({% post_url 2018-03-15-preface-to-passit-2-why-a-password-manager %})
- [Part 3: Why use an open source app?]({% post_url 2018-03-28-preface-to-passit-3-why-open-source %})
- Part 4: Why use a cloud-based password manager?
- [Part 5: Why pick Passit?]({% post_url 2018-04-18-preface-to-passit-5-why-passit %})
