---
layout: post
title:  "Picking a good password"
author: Brendan Berkley
date:   2018-10-12 00:00:00 -0400
categories: ideas
excerpt: >
    The password you pick for your Passit account will protect all of your other
    passwords, so it's really important to pick a good one.
---

Passit is made to withstand attacks on multiple fronts. However, one part of the security model is mostly in your hands: the password.

### How can I make a good password?

We recommend a password that is long, unique, memorable, and impersonal, in that order.

- **Long** means that random hackers with fast computers have a harder time hurting you.
- **Unique** means that you don't use this password anywhere else, so if other sites get hacked, your Passit account is still safe.
- **Memorable** means that no one can physically steal it, because you didn't write it down.
- **Impersonal** means that your middle name, your pet's name, or your anniversary can't be used against you.

Why do we make this recommendation? Read on.

### How will someone try to steal my password?

The UK Government's National Cyber Security Centre has [an infographic](https://www.ncsc.gov.uk/guidance/password-guidance-simplifying-your-approach) that presents eight different ways that passwords can be compromised. Three attacks listed (and one that isn't, at least not obviously) are affected by the password you create:

1. **Brute Force**. An attacker will use a computer to make millions of guesses as fast as possible to figure out your password. Passit has some measures of protection against this, but there are a few things you can do on your end:
    - The longer, the better. Each extra character makes brute force attacks exponentially less effective.
    - Use capital and lowercase letters, numbers, and special characters.
    - Don't use common words. Brute-force attackers tend to catch the people who use `Passw0rd!Passw0rd!` more quickly than someone who uses the same mix of uppercase/lowercase/number/special characters but picks a less obvious word.
2. **Using another site's hacked data**. This wasn't strictly in the infographic I linked to. The scenario is something like this: You used the password `1qaz2wsx` for Passit, and you also use it on Site A. Site A is less secure, and gets hacked. Someone figures out your password over there, see it's associated with your email address, and tries it in Passit. It works! You've been hacked.
3. **Manual Guessing**. If I know that your name is Sidney Cameron Sanders, and I know your birthday is on September 20, then I might try `Scs0920!` and other variants. That sort of thing feels better than `Passw0rd!` and is easier to remember, but may not be doing you any favors.
4. **Stealing Passwords**. This refers to the idea that, if you write your password on a sticky note and place it on your monitor, that's opening up a new risk. This doesn't really have much to do with picking a password, other than to note that:
    - If it's easy to remember, then you don't need to write it down in the first place.
    - Passit strongly recommends using a backup code that you can store somewhere, so there is a fallback available in case you forget.

### Are you speaking in absolutes here?

Yes and no. The order of the last two (memorable and impersonal) is highly debatable. Switching them is more secure, but you might own a fire safe that you trust more than your memory. I think that a password you can memorize is better than not using a password manager because you couldn't remember your master password, or don't want to track down the sticky note that has it written down.

However, longer is pretty much always better, and if you pick a password that isn't used anywhere else, attackers will have a harder time with it.

### Any tips to make it easier to pick a password?

If we recommended a particular formula to make a good password for you, then attackers would see it as well, and customize attacks based on it. So we have to paint in broad strokes here.

The easiest way to make a password safer is to add length. To give a general idea, [this calculator](https://www.grc.com/haystack.htm) shows that `aaaaa12345` could take 10.45 hours to crack in an offline fast attack scenario, but `aaaaa123456` could take 2.25 weeks, and `aaaaa1234567` 1.55 years. Take the numbers with a grain of salt, but believe that one extra character could make that big of a difference.

One way to add length while being easier to remember is to use a [passphrase](https://whatisapassphrase.com/) instead of a password. You can still get into trouble if you use common English words, but a long passphrase can be easier to remember than a long password. Use uncommon words, or words in other languages. Nonsensical sentences can help, as can random punctuation.

You can also get a sense of what attackers are trying to guess before they do a brute force attack. Here's a [list of common passwords](https://en.wikipedia.org/wiki/List_of_the_most_common_passwords), compiled from data breaches over the years. If you do something like these, you're opening yourself up for trouble.
